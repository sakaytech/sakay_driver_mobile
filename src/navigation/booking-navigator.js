/**
 * @format
 * @flow
 */

import React from 'react';
import { createStackNavigator } from 'react-navigation';
import BookingHome from '../screen/booking';

type Props = {
  navigation: any
}

const routeConfigs = {
  Booking: {
    screen: BookingHome
  }
};
const stackNavigatorConfig = {
  initialRouteName: 'Booking'
};

/**
 *
 * @reference https://reactnavigation.org/docs/en/stack-navigator.html#docsNav
 * @type {NavigationContainer}
 */
const BookingNavigator = createStackNavigator(routeConfigs, stackNavigatorConfig);

class BookingScreen extends React.Component<Props> {
  static router = BookingNavigator.router;
  
  render () {
    return <BookingNavigator navigation={this.props.navigation}/>;
  }
}

export default BookingScreen;

/**
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import LoginScreen from '../screen/authentication/login';

type Props = {
  navigation: any
}

const routeConfigs = {
  Login: {
    screen: LoginScreen
  }
};

const stackNavigatorConfig = {
  initialRouteName: 'Login'
};

/**
 *
 * @reference https://reactnavigation.org/docs/en/stack-navigator.html#docsNav
 * @type {NavigationContainer}
 */
const AuthenticationNavigator = createStackNavigator(routeConfigs, stackNavigatorConfig);

class AuthenticationScreen extends Component<Props> {
  static router = AuthenticationNavigator.router;
  
  render () {
    return (
      <AuthenticationNavigator navigation={this.props.navigation}/>
    );
  }
}

export default AuthenticationScreen;
